# Storefront SPA

### Setup & Install

Run `npm install`

### Run

Run `npm start` and then visit http://localhost:8000

### Test

* Run `npm run test` to start unit tests and watch for file changes
* Run `npm run test-single-run` to run unit tests for one time
* Run `npm start` to start application and then in another terminal run `npm run protractor` to run e2e tests

### Compromises/shortcuts made due to time considerations

* Used https://github.com/angular/angular-seed for faster build setup
* Used Material Icons from Google for icons
* Used Foundation 5 styles sheets for faster styling with responsive grid
* Could have used sass instead, by
  * separating single css file into different scss files for different components
  * moving repeated css values into sass variables from a global 'settings' scss file
* Could have implemented more unit tests for views & components
* Responsiveness could have been improved based on designs from `Responsive.md`
