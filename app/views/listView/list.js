'use strict';

angular.module('storefront.list', ['ngRoute', 'storefront.cartService', 'storefront.dataService'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/list', {
      templateUrl: 'views/listView/list.html',
      controller: 'listCtrl',
      controllerAs: 'listCtrl'
    });
  }])

  .controller('listCtrl', ['cartService', 'dataService', '$location', function(cartService, dataService, $location) {
    var ctrl = this;

    dataService.product.list().$promise
      .then(function(products) {
        ctrl.products = products;
      });

    ctrl.addToCart = cartService.addItem;

    ctrl.viewDetails = function(product) {
      $location.url('/product-details/' + product.title)
    }
  }]);