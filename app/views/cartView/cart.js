'use strict';

angular.module('storefront.cart', ['ngRoute', 'storefront.cartService', 'storefront.dataService'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/cart', {
      templateUrl: 'views/cartView/cart.html',
      controller: 'cartCtrl',
      controllerAs: 'cartCtrl'
    });
  }])

  .controller('cartCtrl', ['cartService', 'dataService', '$location', function(cartService, dataService, $location) {
    var ctrl = this;

    ctrl.getItems = cartService.getItems;
    ctrl.getTotal = cartService.getTotal;
    ctrl.removeItem = cartService.removeItem;

    ctrl.setQuantity = function(item, quantity) {
      item.quantity = quantity;
    };

    ctrl.continueShopping = function() {
      $location.url('/list');
    };
  }]);