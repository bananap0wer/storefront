'use strict';

angular.module('storefront.productDetails', ['ngRoute', 'storefront.cartService'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/product-details/:title', {
      templateUrl: 'views/productDetailsView/productDetails.html',
      controller: 'detailsCtrl',
      controllerAs: 'detailsCtrl'
    });
  }])

  .controller('detailsCtrl', ['cartService', 'dataService', '$routeParams', function(cartService, dataService, $routeParams) {
    var ctrl = this;
    var title = $routeParams.title.toLowerCase();

    dataService.product.list().$promise
      .then(function(products) {
        products.some(function(product) {
          if (product.title.toLowerCase() === title) {
            ctrl.product = product;
            return true;
          }
        });
      });

    ctrl.quantity = 1;

    ctrl.setQuantity = function(number) {
      ctrl.quantity = number;
    };

    ctrl.addToCart = function() {
      cartService.addItem(ctrl.product, ctrl.quantity);
    };
  }]);