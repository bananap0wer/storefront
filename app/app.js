'use strict';

angular.module('storefront', [
    'ngRoute',
    'storefront.preventDefault',
    'storefront.cart',
    'storefront.cartSummary',
    'storefront.headerNav',
    'storefront.list',
    'storefront.numPicker',
    'storefront.productDetails',
    'storefront.productListItem',
    'storefront.cartService',
    'storefront.dataService'
  ])

  .config(['$locationProvider', '$routeProvider', '$compileProvider', function($locationProvider, $routeProvider, $compileProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/list'});
    $compileProvider.debugInfoEnabled(true);
  }]);
