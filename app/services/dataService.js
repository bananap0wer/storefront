'use strict';

angular.module('storefront.dataService', ['ngResource'])
  .service('dataService', ['$resource', function($resource) {
    var api = {};

    // In real world scenario, the URL would point to an endpoint that returns real data dynamically instead of from
    // a static file
    api.product = $resource('data/products.json', null, {
      list: {
        method: 'GET',
        isArray: true
      }
    });

    return api;
  }]);
