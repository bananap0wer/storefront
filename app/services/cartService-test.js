'use strict';

describe('storefront.cartService module', function() {
  var cartService;
  var mockProduct1;
  var mockProduct2;
  var items;

  beforeEach(module('storefront.cartService'));

  beforeEach(inject(function(_cartService_) {
    cartService = _cartService_;
    // The mock products could be loaded from fixture files
    mockProduct1 = {
      "title": "Blue Stripe Stoneware Plate",
      "brand": "Kiriko",
      "price": 40,
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at purus pulvinar, placerat turpis ac, interdum metus. In eget massa sed enim hendrerit auctor a eget.",
      "image": "blue-stripe-stoneware-plate.jpg"
    };
    mockProduct2 = {
      "title": "Hand Painted Blue Flat Dish",
      "brand": "Kiriko",
      "price": 28,
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at purus pulvinar, placerat turpis ac, interdum metus. In eget massa sed enim hendrerit auctor a eget arcu. Curabitur ac pharetra nisl, sit amet mattis dolor.",
      "image": "hand-painted-blue-flat-dish.jpg"
    };
    items = cartService.getItems();
  }));

  describe('cartService service', function() {

    describe('addItem', function() {
      it('should add product with default quantity', function() {
        cartService.addItem(mockProduct1);

        expect(items[0].quantity).toBe(1);
        expect(items[0].product).toBe(mockProduct1);
        expect(items.length).toBe(1);
      });

      it('should add product with given quantity', function() {
        cartService.addItem(mockProduct1, 5);

        expect(items[0].quantity).toBe(5);
        expect(items.length).toBe(1);
      });

      it('should increase quantity of existing product', function() {
        var addedItem;

        cartService.addItem(mockProduct1);

        addedItem = items[0];

        expect(addedItem.quantity).toBe(1);

        cartService.addItem(mockProduct1);

        expect(addedItem.quantity).toBe(2);

        cartService.addItem(mockProduct1, 3);

        expect(addedItem.quantity).toBe(5);
      });

      it('should not create new items when adding the same products', function() {
        cartService.addItem(mockProduct1);
        cartService.addItem(mockProduct1);

        expect(items.length).toBe(1);

        cartService.addItem(mockProduct2);
        cartService.addItem(mockProduct2);
        cartService.addItem(mockProduct2);

        expect(items.length).toBe(2);
      });
    });

    describe('removeItem', function() {
      it('should remove items that match the provided product', function () {
        cartService.addItem(mockProduct1);
        cartService.addItem(mockProduct2, 2);

        cartService.removeItem(mockProduct1);

        expect(items.length).toBe(1);

        cartService.removeItem(mockProduct2);

        expect(items.length).toBe(0);
      });
    });

    describe('getTotal', function() {
      it('should return the correct total price of products in cart', function () {
        var product1Quantity = 5;
        var product2Quantity = 2;
        var total = mockProduct1.price * product1Quantity + mockProduct2.price * product2Quantity;

        cartService.addItem(mockProduct1, product1Quantity);
        cartService.addItem(mockProduct2, product2Quantity);

        expect(cartService.getTotal()).toBe(total);
      });
    });

  });
});
