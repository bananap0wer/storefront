'use strict';

angular.module('storefront.cartService', [])
  .service('cartService', function() {
    var api = {};
    var items = [];

    function findItemIndex(product) {
      var itemIndexFound = -1;

      items.some(function(item, index) {
        // Assuming here that `titles` are always unique. In real-world situations, there would be an ID field.
        if (item.product.title === product.title) {
          itemIndexFound = index;
          return true;
        }
      });

      return itemIndexFound;
    }

    api.getItems = function() {
      return items;
    };

    api.addItem = function(product, quantity) {
      var itemIndexFound = findItemIndex(product);

      quantity = quantity || 1;

      if (itemIndexFound > -1) {
        items[itemIndexFound].quantity += quantity;
        return;
      }

      items.push({
        product: product,
        quantity: quantity
      });
    };

    api.removeItem = function(product) {
      var itemIndexFound = findItemIndex(product);

      if (itemIndexFound > -1) {
        items.splice(itemIndexFound, 1);
      }
    };

    api.getTotal = function() {
      return items.reduce(function(acc, item) {
        return acc + item.product.price * item.quantity;
      }, 0);
    };

    return api;
  });
