'use strict';

var EVENTS = {
  click: 'click'
};

var CLASSES = {
  open: 'open'
};

angular.module('storefront.cartSummary', ['storefront.cartService'])
  .component('cartSummary', {
    templateUrl: 'components/cartSummary/cartSummary.html',
    controller: ['cartService', '$document', '$element', '$location', '$timeout', function(cartService, $document, $element, $location, $timeout) {
      var ctrl = this;
      var $popup;

      function openCartPopup() {
        if ($popup.hasClass(CLASSES.open)) {
          return;
        }

        $popup.addClass(CLASSES.open);
        $timeout(function() {
          //$timeout here so that the even handler is only registered after the current digest
          $document.on(EVENTS.click, clickOutsidePopup);
        });
      }

      function closeCartPopup() {
        $document.off(EVENTS.click, clickOutsidePopup);
        $popup.removeClass(CLASSES.open);
      }

      function clickOutsidePopup(e) {
        if (angular.element(e.target).closest($popup).length === 0) {
          closeCartPopup();
        }
      }

      ctrl.$postLink = function() {
        $popup = $element.find('.cart-popup');
      };

      ctrl.$onDestroy = closeCartPopup;

      ctrl.openCartPopup = openCartPopup;
      ctrl.getItems = cartService.getItems;
      ctrl.getTotal = cartService.getTotal;

      ctrl.removeItem = function(product) {
        $timeout(function() {
          //$timeout here to allow `clickOutsidePopup` to be triggered first before removing the item
          cartService.removeItem(product);
        });
      };

      ctrl.getItemsCount = function() {
        return cartService.getItems().reduce(function(acc, item) {
          return acc + item.quantity;
        }, 0);
      };

      ctrl.viewCart = function() {
        $location.url('/cart');
      };
    }]
  });
