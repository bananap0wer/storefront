'use strict';

angular.module('storefront.productListItem', [])
  .component('productListItem', {
    bindings: {
      product: '<',
      onAddToCart: '&',
      onViewDetails: '&'
    },
    templateUrl: 'components/productListItem/productListItem.html',
    controller: [function() {
      var ctrl = this;

      ctrl.addToCart = function() {
        ctrl.onAddToCart({$event: {product: ctrl.product}});
      };

      ctrl.viewDetails = function() {
        ctrl.onViewDetails({$event: {product: ctrl.product}});
      };
    }]
  });
