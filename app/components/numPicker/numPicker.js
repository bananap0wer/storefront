'use strict';

angular.module('storefront.numPicker', [])
  .component('numPicker', {
    templateUrl: 'components/numPicker/numPicker.html',
    bindings: {
      number: '<',
      onNumberChange: '&'
    },
    controller: function() {
      var ctrl = this;

      function emitChangedNumber() {
        ctrl.onNumberChange({$event: {number: ctrl.number}});
      }

      ctrl.$onInit = function() {
        ctrl.number = ctrl.number || 0;
      };

      ctrl.add = function() {
        ctrl.number++;
        emitChangedNumber();
      };

      ctrl.remove = function() {
        ctrl.number--;
        if (ctrl.number < 1) {
          ctrl.number = 1;
        }
        emitChangedNumber();
      };
    }
  });
