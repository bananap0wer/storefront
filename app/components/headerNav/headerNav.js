'use strict';

angular.module('storefront.headerNav', [])
  .component('headerNav', {
    templateUrl: 'components/headerNav/headerNav.html'
  });
