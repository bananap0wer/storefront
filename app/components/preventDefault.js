'use strict';

var EVENT = {
  click: 'click',
  $destroy: '$destroy'
};

function preventDefaultHandler(e) {
  e.preventDefault();
}

angular.module('storefront.preventDefault', [])

  .directive('preventDefault', function() {
    return function(scope, elm) {
      elm.on(EVENT.click, preventDefaultHandler);

      scope.$on(EVENT.$destroy, function() {
        elm.off(EVENT.click, preventDefaultHandler);
      })
    };
  });
