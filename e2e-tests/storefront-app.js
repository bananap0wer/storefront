'use strict';

describe('storefront app', function() {

  function assertViewRendered(path, expectedViewClass) {
    browser.get('index.html#!' + path);
    expect(element.all(by.css('[ng-view] > .' + expectedViewClass)).first().isPresent()).
    toBe(true);
  }

  it('should automatically redirect to /list when location hash is empty', function() {
    browser.get('/');
    expect(browser.getLocationAbsUrl()).toMatch("/list");

    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/list");
  });

  describe('list view', function() {

    it('should render when user navigates to /list', function() {
      assertViewRendered('/list', 'list-view');
    });

  });

  describe('cart view', function() {

    it('should render when user navigates to /cart', function() {
      assertViewRendered('/cart', 'cart-view');
    });

  });

  describe('product-detail view', function() {

    it('should render when user navigates to /product-details/title', function() {
      assertViewRendered('/product-details/title', 'product-details-view');
    });

  });

});
